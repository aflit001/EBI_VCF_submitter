samplefile      = "rpc_sample_names.txt"
xml_template    = "rpc_template.xml"
spp_codes_file  = "rpc_spp_codes.csv"
outfolder       = 'fixed'
center_name     = 'PRI'
proj_name       = os.path.abspath(os.path.curdir).replace(' ', '_').replace('/', '_').replace('\\', '_').replace('___', '_').replace('__', '_').strip('_')


chromosomeAlias = {
	"SL2.40ch01": "CM001064.1",
	"SL2.40ch02": "CM001065.1",
	"SL2.40ch03": "CM001066.1",
	"SL2.40ch04": "CM001067.1",
	"SL2.40ch05": "CM001068.1",
	"SL2.40ch06": "CM001069.1",
	"SL2.40ch07": "CM001070.1",
	"SL2.40ch08": "CM001071.1",
	"SL2.40ch09": "CM001072.1",
	"SL2.40ch10": "CM001073.1",
	"SL2.40ch11": "CM001074.1",
	"SL2.40ch12": "CM001075.1",
}


forbiddenChroms = [ 'SL2.40ch00' ]



#xml_conf = {
#    "ANALYSIS": {
#        "alias"      : "morex_haruna_nijo",
#        "center_name": "Plant Research International",
#        "broker_name": "ENSEMBL GENOMES",
#        "accession"  : "ERZ016065", # analysis id
#        "IDENTIFIERS": {
#            "PRIMARY_ID": "ERZ016065",
#            "SUBMITTER_ID": {
#                "namespace": "IPK-Gatersleben",
#                "_val"     : "morex_haruna_nijo"
#            }
#        },
#        "TITLE"      : "Variations called from whole genome shotgun survey sequencing of Barley cv. Haruna Nijo for de novo assembly of gene-space",
#        "DESCRIPTION": "Variations called as described in: A physical, genetic and functional sequence assembly of the barley genome, Nature 2012, 491:711, PMID:23075845",
#        "STUDY_REF"  : {
#            "accession": "ERP001451",
#            "refname"  : "Haruna Nijo_WGS",
#            "refcenter": "IPK-Gatersleben",
#            "IDENTIFIERS": {
#                "PRIMARY_ID": "ERP001451",
#                "SUBMITTER_ID": {
#                    "namespace": "IPK-Gatersleben",
#                    "_val"     : "Haruna Nijo_WGS"
#                }
#            }
#        },
#        "SAMPLE_REF": {
#            "accession": "ERS140602",
#            "label"    : "MAPPING_harunaNijoMOREX_BWAv1.sorted.rmdup.bam",
#            "refname"  : "Haruna Nijo",
#            "refcenter": "IPK-Gatersleben",
#            "IDENTIFIERS": {
#                "PRIMARY_ID": "ERS140602",
#                "SUBMITTER_ID": {
#                    "namespace": "IPK-Gatersleben",
#                    "val"      : "Haruna Nijo"
#                }
#            }
#        },
#        "ANALYSIS_TYPE": {
#            "SEQUENCE_VARIATION": {
#                "ASSEMBLY": {
#                    "STANDARD": {
#                        "accession": "GCA_000188115.1"
#                    }
#                },
#                #"SEQUENCE": {
#                    #"accession": "CAJW010010000.1",
#                    #"label"    :"contig_10000"
#                #},
#                "EXPERIMENT_TYPE": "Whole genome sequencing"
#            }
#        },
#        "FILES": {
#            "FILE": {
#                "filename"       : "morex:haruna_nijo.final.vcf",
#                "filetype"       : "vcf",
#                "checksum_method": "MD5",
#                "checksum"       : "5d6017fab64f8a9160e1aac3d7276e48"
#            }
#        },
#        "ANALYSIS_LINKS": {
#            "ANALYSIS_LINK": {
#                "XREF_LINK": {
#                    "DB": "ENA-SUBMISSION",
#                    "ID": "ERA256779"
#                }
#            }
#        }
#    }
#}


#pprint.pprint( chromosomeAlias )
#pprint.pprint( xml_conf )

#print chromosomeAlias
#pprint.pprint( xml_conf )


##RF_001 S.lyc LA27061
#sample reference  ERS398435
#assembly acession GCA_000188115.1
#assembly link     ftp://ftp.solgenomics.net/genomes/Solanum_lycopersicum/assembly/build_2.40/S_lycopersicum_chromosomes.2.40.fa.gz

#pprint.pprint( xml_conf )
#sys.exit(0)


#sys.exit(0)


#sys.exit(0)
