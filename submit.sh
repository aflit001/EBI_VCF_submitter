FILE=$1

source credentials

#TEST URL
DST="https://www-test.ebi.ac.uk/ena/submit/drop-box/submit"
#REAL URL
#DST="https://www.ebi.ac.uk/ena/submit/drop-box/submit"

LTYPE="ENA"
CMD="curl -k -F \"SUBMISSION=@$FILE.xml.submission.xml\" \
	-F \"ANALYSIS=@$FILE.xml\" \
	\"$DST/?auth=$LTYPE%20$LOGIN%20$PASS\" \
	| xmllint --format - | \
	tee $FILE.xml.submission.xml.receipt.xml"

echo $CMD
