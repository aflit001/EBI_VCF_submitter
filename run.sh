find . -maxdepth 1 -name '*.vcf.gz' | sort | xargs -I{} bash -c 'echo -en "{}\t"; gunzip -c {} | head -100 | grep "#CHROM" | cut -f 10' | tee rpc_sample_names.txt

mkdir fixed

rm fixed/*

./fixVcf.py *.vcf.gz

cd fixed

../submit.sh application

#for file in *.vcf.gz; do
#	echo $file;
#
#	if [[ ! -f fixed/$file ]]; then
#		./fixVcf.py $file | gzip -1 -c > fixed/$file;
#	fi
#
#	#if [[ ! -f "fixed/$file.md5" ]]; then
#	#	echo "calculating md5"
#	#	md5sum fixed/$file | tee fixed/$file.md5
#	#fi
#done;
